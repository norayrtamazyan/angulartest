'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$scope', '$http', '$templateCache', function($scope, $http, $templateCache) {

    $scope.data = [];
    $scope.skip = 0;
    $scope.sort = 'none';
    $scope.limit = 20;
    $scope.loadMore = true;

    $scope.ff = function () {
        $scope.sort = $scope.data.singleSelect;
        $scope.skip = 0;
        $scope.limit = 20;
        getData();
    };
    $scope.loadMoreData = function() {
        $scope.skip += $scope.limit;
        getData()
    }


        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                $scope.loadMoreData();
            }
        });
    var nums = [1,2,3,4,5,6,7,8,9,10],
        ranNums = [],
        i = nums.length,
        j = 0;

    while (i--) {
        j = Math.floor(Math.random() * (i+1));
        ranNums.push(nums[j]);
        nums.splice(j,1);
    }

    $scope.imgSrcs = ranNums;

   getData();
        function getData() {
            if (!$scope.loadMore) {
                return;
            }
            $scope.method = 'GET';
            $scope.url = 'http://localhost:3000/api/products?limit=' + $scope.limit + '&skip=' + $scope.skip + '&sort=' + $scope.sort ;
            $scope.code = null;
            $scope.response = null;
            $http({method: $scope.method, url: $scope.url, cache: $templateCache}).
            then(function(response) {
                $scope.loadMore = !!response.data.length;
                $scope.status = response.status;
                if($scope.skip == 0){
                    $scope.data = response.data;
                } else {
                    $scope.data = $scope.data.concat(response.data);
                }
                $scope.data.singleSelect = $scope.sort;

                $scope.data.forEach(function (item) {
                    item.displayDate = moment(item.date).fromNow();
                })
                console.log($scope.data);
            }, function(response) {
                $scope.data = response.data || 'Request failed';
                $scope.status = response.status;
            });

        }


    //$scope.updateModel = function(method, url) {
       // $scope.method = method;
        //$scope.url = url;
    //};

}]);
